//
//  DeviceDetailViewModel.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class DeviceDetailViewModel {

    //------------------------------------
    //  MARK: - Limited access properties
    //------------------------------------

    /// Device information object.
    private (set) var device: Device
    
    //------------------------------------
    //  MARK: - Initializers
    //------------------------------------
    
    init(device: Device) {
        self.device = device
    }
    
    //------------------------------------
    //  MARK: - Public methods
    //------------------------------------

    /// Request a map snapshot with location of device and custom annotation view.
    ///
    /// - Parameters:
    ///   - size: Size of image.
    ///   - completion: A closure to be executed once the request has finished.
    func getMapSnapshot(size: CGSize, completion: @escaping ((_ snapshot: UIImage?) -> Void)) {
        
        let options = MKMapSnapshotter.Options()
        options.region = MKCoordinateRegion(center: device.coordinate,
                                            latitudinalMeters: 250,
                                            longitudinalMeters: 250)
        options.size = size
        
        let snapShotter = MKMapSnapshotter(options: options)
        snapShotter.start(with: DispatchQueue.global(qos: .background), completionHandler: { [weak self] (snapshot, error) in
            
            guard error == nil else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            guard let snapshot = snapshot else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            guard let self = self else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            let annotationView = DeviceAnnotationView(annotation: DeviceAnnotation(device: self.device),
                                                      reuseIdentifier: "annotation")

            guard let pinImage = annotationView.image else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }

            let coordinatePoint = snapshot.point(for: self.device.coordinate)
            
            UIGraphicsBeginImageContextWithOptions(snapshot.image.size, true, snapshot.image.scale)
            snapshot.image.draw(at: CGPoint.zero)
            
            let fixedPinPoint = CGPoint(x: coordinatePoint.x - pinImage.size.width / 2,
                                        y: coordinatePoint.y - pinImage.size.height)
            pinImage.draw(at: fixedPinPoint)
            
            let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
            
            DispatchQueue.main.async {
                completion(snapshotImage)
            }
            UIGraphicsEndImageContext()
            
        })
        
    }
    
}
