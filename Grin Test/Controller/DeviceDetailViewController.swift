//
//  DeviceDetailViewController.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit
import MaryPopin

class DeviceDetailViewController: UIViewController {
    
    //------------------------------------
    //  MARK: - IBOutlets
    //------------------------------------

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var snapshotImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //------------------------------------
    //  MARK: - View Model
    //------------------------------------
    
    var viewModel: DeviceDetailViewModel!
    
    //------------------------------------
    //  MARK: - Controller Life Cycle Methods
    //------------------------------------
    
    var dismissBlockCompletion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        
        idLabel.text = viewModel.device.id
        nameLabel.text = viewModel.device.name
        strengthLabel.text = viewModel.device.strength
        createdAtLabel.text = viewModel.device.createdAt?.toString(with: "MMM d, h:mm a")
        
        activityIndicator.startAnimating()
        viewModel.getMapSnapshot(size: snapshotImageView.frame.size) { (image) in
            self.activityIndicator.stopAnimating()
            self.snapshotImageView.image = image
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dismissBlockCompletion?()
    }

}

extension DeviceDetailViewController {
    
    /// Class function to get an inicialize instance with device information. See more in https://github.com/Backelite/MaryPopin
    ///
    /// - Parameters:
    ///   - device: Device object to show detail information.
    ///   - dismissBlockCompletion: Completion block that called when pop view controller is dismissed.
    /// - Returns: DeviceDetailViewController instance.
    class func popinViewController(device: Device,
                                   dismissBlockCompletion: (() -> Void)? = nil) -> DeviceDetailViewController {
        
        let deviceDetailVC = DeviceDetailViewController()
        deviceDetailVC.viewModel = DeviceDetailViewModel(device: device)
        deviceDetailVC.dismissBlockCompletion = dismissBlockCompletion
        deviceDetailVC.setPopinAlignment(.centered)
        deviceDetailVC.setPopinOptions([deviceDetailVC.popinOptions()])
        deviceDetailVC.setPopinTransitionDirection(.bottom)
        return deviceDetailVC
        
    }
    
}
