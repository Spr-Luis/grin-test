//
//  BluetoothScannerViewController.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit
import JTProgressHUD

class BluetoothScannerViewController: UIViewController {

    //------------------------------------
    //  MARK: - IBOutlets
    //------------------------------------

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reloadButton: ReloadButton!
    
    //------------------------------------
    //  MARK: - View Model
    //------------------------------------
    
    var viewModel: BluetoothScannerViewModel = BluetoothScannerViewModel()
    
    //------------------------------------
    //  MARK: - Controller Life Cycle Methods
    //------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(BluetoothDeviceCell.self)
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView()
        
        viewModel.didStopScanBlock = {
            self.reloadButton.stopLoading()
        }
        
        viewModel.shouldReloadView = {
            self.tableView.reloadSections([0], with: .fade)
        }
        
        viewModel.bluetoothDisabledBlock = {
            let error = GrinError(localizedTitle: "Bluetooth is disabled",
                                  localizedDescription: "Please turn on your bluetooth.")
            self.showError(error: error)
            self.reloadButton.stopLoading()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reload(reloadButton)
    }

    //------------------------------------
    //  MARK: - IBActions
    //------------------------------------
    
    @IBAction func reload(_ sender: Any) {
        reloadButton.startLoading()
        viewModel.scan()
    }
    
}

extension BluetoothScannerViewController {
    
    /// Alert to ask if user want save the device information.
    ///
    /// - Parameter device: Device information object.
    private func shouldMustBeSaved(device: Device) {
        
        let alert = UIAlertController(title: "⚠️ Alert ⚠️",
                                      message: "Do you want save the device?",
                                      preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) { (_) in
            self.save(device: device)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel,
                                         handler: nil)
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    /// Save device information
    ///
    /// - Parameter device: Device information object.
    private func save(device: Device) {
        JTProgressHUD.show()
        viewModel.save(device: device) { [weak self] (error) in
            guard let self = self else { return }
            if let error = error {
                JTProgressHUD.hide {
                    self.showError(error: error)
                }
            } else {
                JTProgressHUD.hide {
                    self.showAlert(message: "The device was saved!")
                }
            }
        }
    }
    
}

//------------------------------------
//  MARK: - UITableViewDataSource & UITableViewDelegate
//------------------------------------

extension BluetoothScannerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 30))
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.text = String(format: "%d found devices!", viewModel.devices.count)
        label.textAlignment = .center
        label.textColor = .darkGray
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return viewModel.devices.isEmpty ? 0 : 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.devices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as BluetoothDeviceCell
        cell.config(with: viewModel.devices[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        shouldMustBeSaved(device: viewModel.devices[indexPath.row])
    }
    
}
