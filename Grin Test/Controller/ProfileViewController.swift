//
//  ProfileViewController.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    //------------------------------------
    //  COMMENT: - El uso de pods como 'TPKeyboardAvoing' fue únicamente para hacer que la interacción del texto en el UITextView y el keyboard se viera fluido. Bien puede hacerse sin el pod, usando UIScrollView y los constraints necesarios.
    //------------------------------------
    
    //------------------------------------
    //  MARK: - IBOutlets
    //------------------------------------

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var optionsContainerView: UIView!
    
    //------------------------------------
    //  MARK: - Controller Life Cycle Methods
    //------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.async {
            
            self.updateAvatarImageViewLayout()
            self.updateAlertViewShadow()
            
        }
        
    }
    
}

extension ProfileViewController {
    
    /// Update Avatar Image View with  circle layer.
    private func updateAvatarImageViewLayout() {
        
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width/2
        avatarImageView.layer.masksToBounds = true

    }

    /// Update Alert View with shadow layer.
    private func updateAlertViewShadow() {
        
        let shadowRadius: CGFloat = 3.0
        
        optionsContainerView.layer.masksToBounds = false
        optionsContainerView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        optionsContainerView.layer.shadowRadius = shadowRadius
        optionsContainerView.layer.shadowOpacity = 2.5
        
        var viewWidth = optionsContainerView.frame.width
        var viewHeight = optionsContainerView.frame.height
        
        viewHeight -= (shadowRadius + 2.0)
        viewWidth -= (shadowRadius + 2.0)
        
        let frame = optionsContainerView.bounds
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: frame.minX + shadowRadius, y: frame.minY))
        path.addLine(to: CGPoint(x: frame.minX + shadowRadius, y: viewHeight))
        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
        path.addLine(to: CGPoint(x: viewWidth, y: frame.minY))
        
        path.close()
        
        optionsContainerView.layer.shadowPath = path.cgPath
        
    }
    
}
