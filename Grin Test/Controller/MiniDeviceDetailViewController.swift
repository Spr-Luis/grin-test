//
//  MiniDeviceDetailViewController.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/27/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit

class MiniDeviceDetailViewController: UIViewController {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowRadius = 3
        view.layer.shadowOpacity = 0.5
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = false

    }
    
    func config(device: Device) {
        idLabel.text = device.id
        nameLabel.text = device.name + "\n" + "Texto para que se vea más larga la vista. :)"
        strengthLabel.text = device.strength
        createdAtLabel.text = device.createdAt?.toString(with: "MMM d, h:mm a")
    }
    
}
