//
//  ItemView.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit

@IBDesignable
class ItemView: UIView {
    
    //------------------------------------
    //  MARK: - Properties
    //------------------------------------
    
    /// Color of circle view.
    @IBInspectable var indicatorColor: UIColor = .white {
        didSet {
            indicatorView.backgroundColor = indicatorColor
        }
    }
    
    /// Title for Item View
    @IBInspectable var titleText: String = "Title" {
        didSet {
            titleLabel.text = titleText
        }
    }
    /// Description for Item View
    @IBInspectable var descriptionText: String = "Description" {
        didSet {
            descriptionLabel.text = descriptionText
        }
    }
    
    /// Corner radius for Item View
    @IBInspectable var cornerRadius: CGFloat = 10.0 {
        didSet {
            updateLayer()
        }
    }
    
    /// Border width for Item View
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            updateLayer()
        }
    }
    
    /// Border color for Item View
    @IBInspectable var borderColor: UIColor = .darkGray {
        didSet {
            updateLayer()
        }
    }
    
    /// Background color for Item View
    @IBInspectable var containerBackgroundColor: UIColor = .lightGray {
        didSet {
            containerView.backgroundColor = containerBackgroundColor
        }
    }
    
    //------------------------------------
    //  MARK: - Overloaded methods
    //------------------------------------

    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayer()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
    }
    
    //------------------------------------
    //  MARK: - IBOutlets
    //------------------------------------

    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //------------------------------------
    //  MARK: - Initializers
    //------------------------------------

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private var containerView: UIView!
    
    //------------------------------------
    //  MARK: - Private methods
    //------------------------------------
    
    /// Update the layer from layer properties.
    private func updateLayer() {
        indicatorView.layer.cornerRadius = indicatorView.frame.size.width/2.0
        indicatorView.layer.masksToBounds = true
        containerView.layer.cornerRadius = cornerRadius
        containerView.layer.borderWidth = borderWidth
        containerView.layer.borderColor = borderColor.cgColor
        containerView.layer.masksToBounds = true
    }
    
    /// Setup view from XIB file.
    private func setup() {
        backgroundColor = UIColor.clear
        containerView = loadViewFromNib()
        containerView.frame = bounds
        containerView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,
                                          UIView.AutoresizingMask.flexibleHeight]
        addSubview(containerView)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ItemView", bundle: bundle)
        guard let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView else {
            return UIView()
        }
        return view
        
    }

}
