//
//  BluetoothDeviceCell.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit

class BluetoothDeviceCell: UITableViewCell {
    
    //------------------------------------
    //  MARK: - IBOutlets
    //------------------------------------

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var strengthLabel: UILabel!

    /// Update the cell with the device information.
    ///
    /// - Parameter device: Device information object.
    func config(with device: Device) {
        idLabel.text = device.id
        nameLabel.text = device.name
        strengthLabel.text = device.strength
    }
    
}
