//
//  DeviceAnnotationView.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import MapKit

class DeviceAnnotationView: MKAnnotationView {

    //------------------------------------
    //  MARK: - Initializers
    //------------------------------------

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {

        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        image = UIImage(named: "bluetooth_marker")
        centerOffset = CGPoint(x: 0, y: -bounds.height/2)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //------------------------------------
    //  MARK: - Overloaded methods
    //------------------------------------
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        
        if selected {
            image = UIImage(named: "bluetooth_marker_selected")
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowRadius = 3
            layer.shadowOpacity = 0.5
        } else {
            image = UIImage(named: "bluetooth_marker")
            layer.shadowColor = UIColor.clear.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowRadius = 0
            layer.shadowOpacity = 0.0
        }
        
    }
    
}
