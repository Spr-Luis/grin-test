//
//  API.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import Alamofire

//------------------------------------
//  MARK: - Protocols for our API :)
//------------------------------------

protocol Path {
    
    var path: String { get }
    var method: Alamofire.HTTPMethod { get }
    var url: URL { get }
    var headers: HTTPHeaders? { get }
    var encoding: ParameterEncoding { get }
    func request(parameters: Parameters?) -> DataRequest
    
}

extension Path {
    
    func request(parameters: Parameters? = nil) -> DataRequest {
        return request(parameters: parameters)
    }
    
}

protocol APIBase: Path {
    
    var appURL: URL { get }
    var name: String { get }
    
}

//------------------------------------
//  MARK: - API Endpoints
//------------------------------------

enum API {
    
    case bluetoothAll
    case bluetoothCreate
    
}

extension API: APIBase {
    
    internal var appURL: URL {
        return URL(string: "http://mock.westcentralus.cloudapp.azure.com/\(name)/")!
    }
    
    internal var name: String {
        return "grin_test"
    }
    
}

extension API: Path {
    
    var path: String {
        
        switch self {
        case .bluetoothAll:         return "bluetooth/all"
        case .bluetoothCreate:      return "bluetooth/create"
        }
        
    }
    
    var method: Alamofire.HTTPMethod {
        
        switch self {
        case .bluetoothCreate:
            return .post
        default:
            return .get
        }
        
    }
    
    var url: URL {
        return appURL.appendingPathComponent(path)
    }
    
    var headers: HTTPHeaders? {
//        Exmaple
//        return ["Authorization": "Bearer " + "key"]
        return nil
        
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .bluetoothAll:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    func request(parameters: Parameters? = nil) -> DataRequest {
        
        let message = """
                            SERVICE - \(String(describing: self).capitalized)
                            URL: \(url.absoluteString)
                            METHOD: \(method.rawValue)
                            PARAMETERS: \(parameters ?? [:])
                            HEADERS: \(headers ?? [:])
                        """
        log.debug(message)
        
        let request = Alamofire.request(url,
                                        method: method,
                                        parameters: parameters,
                                        encoding: encoding,
                                        headers: headers)
        
        //------------------------------------
        //  CHECK: - We can remplace DataRequest inicialization to use a custom Alamofire.SessionManager to save user request with Instabug. https://docs.instabug.com/docs/ios-logging#section-network-logs
        //------------------------------------
        
        return request
        
    }
    
}
