//
//  APIResponse.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import ObjectMapper

/// API Response status as a enum.
///
/// - ok: Success request.
/// - fail: Fail request.
enum APIResponseStatus: String {
    case success = "OK"
    case fail = "FAIL"
}

/// APIResponse Class works as a base, we use a generic to use only a class for serialize all responses.
class APIResponse<T: Mappable>: Mappable {
    
    private (set) var objects: [T] = []
    private (set) var object: T?
    private (set) var status: APIResponseStatus = .success
    private (set) var date: Date?
    private (set) var message: String?
    private (set) var code: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        objects <- map["objects"]
        object <- map["object"]
        status <- (map["status"], EnumTransform<APIResponseStatus>())
        message <- map["message"]
        code <- map["code"]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        date <- (map["datetime"], DateFormatterTransform(dateFormatter: dateFormatter))

    }
    
}
