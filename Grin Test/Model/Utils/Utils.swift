//
//  Utils.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/21/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import UIKit
import JTProgressHUD

//------------------------------------
//  CHECK: - File with some utils.
//------------------------------------

extension Date {
    
    func toString(with format: String = "yyyy-MM-dd") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
    
}

extension UIView {
    
    func fadeIn(withDuration duration: TimeInterval = 1.0, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(withDuration duration: TimeInterval = 1.0, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
}

//------------------------------------
//  MARK: - Using protocol you can use the class name as reuse identifier
//------------------------------------

protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
    static var nibName: String { return String(describing: self) }
}

extension UITableViewCell: NibLoadableView { }

protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String { return String(describing: self) }
}

extension UITableViewCell: ReusableView { }

extension UITableView {
    
    func register<T: UITableViewCell>(_: T.Type) {
        
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
    func dequeueReusableCell<T: UITableViewCell>() -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
}

//------------------------------------
//  CHECK: - I love JTProgressHUD, is a 3rd library that you can use for show an HUD as a loader, is fully customizable and easy to use.
//------------------------------------

extension JTProgressHUD {
    
    class func hide(completion: @escaping () -> Void) {
        JTProgressHUD.hide()
        
        let when = DispatchTime.now() + 0.3
        DispatchQueue.main.asyncAfter(deadline: when) {
            completion()
        }
        
    }
    
}

//------------------------------------
//  MARK: - Extension to show errors or alerts with a single line.
//------------------------------------

extension UIViewController {
    
    func showError(error: GrinError,
                   with buttonTitle: String = "OK") {
        let alert = UIAlertController(title: error.localizedTitle,
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        let done = UIAlertAction(title: buttonTitle, style: .default) { (_) in
            
        }
        
        alert.addAction(done)
        present(alert, animated: true, completion: nil)
    }
    
    func showError(error: GrinError,
                   handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: error.localizedTitle, message: error.localizedDescription, preferredStyle: .alert)
        let done = UIAlertAction(title: "OK", style: .default, handler: handler)
        
        alert.addAction(done)
        present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String = "Alert",
                   message: String,
                   with buttonTitle: String = "OK",
                   handler: ((UIAlertAction) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: handler)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
}
