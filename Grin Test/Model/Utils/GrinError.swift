//
//  GrinError.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation

//------------------------------------
//  MARK: - Typealias for error block completion.
//------------------------------------

typealias GrinCompletion = (_ error: GrinError?) -> Void

protocol ErrorProtocol: Error {
    var localizedTitle: String { get }
    var localizedDescription: String { get }
}

//------------------------------------
//  CHECK: - This struct can be used for any error handler.
//------------------------------------

struct GrinError: ErrorProtocol {

    var localizedTitle: String
    var localizedDescription: String
    
    init(localizedTitle: String = "Error",
         localizedDescription: String) {
        self.localizedTitle = localizedTitle
        self.localizedDescription = localizedDescription
    }
    
    init(localizedTitle: String = "Error",
         description: String,
         additionalError: String?) {
        if let additionalError = additionalError {
            self.init(localizedTitle: localizedTitle,
                      localizedDescription: description + "\n" + additionalError)
        } else {
            self.init(localizedTitle: localizedTitle,
                      localizedDescription: description)
        }
    }
    
}
