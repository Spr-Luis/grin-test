//
//  Device.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation
import CoreBluetooth

//------------------------------------
//  CHECK: - Why Object Mapper? Is so easy to use and has support with Alamofire,
//           'Codable' still does not have support with Alamofire (>4.0).
//           https://grokswift.com/decodable-with-alamofire-4/
//------------------------------------

final class Device: Mappable {
    
    private (set) var id: String = ""
    private (set) var createdAt: Date?
    private (set) var strength: String = ""
    private (set) var name: String = ""
    
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    
    private (set) lazy var coordinate: CLLocationCoordinate2D = {
       return CLLocationCoordinate2D(latitude: latitude,
                                     longitude: longitude)
    }()
    
    init?(map: Map) {
        mapping(map: map)
        guard id.isEmpty == false else { return nil }
    }
    
    init(peripheral: CBPeripheral, rssi: String) {
        self.id = peripheral.identifier.uuidString
        self.name = peripheral.name ?? "UNKNOWN DEVICE"
        self.strength = rssi + "dB"
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        strength <- map["strength"]
        name <- map["name"]
        latitude <- map["location.latitude"]
        longitude <- map["location.longitude"]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    
        createdAt <- (map["created"], DateFormatterTransform(dateFormatter: dateFormatter))

    }

}

extension Device: Equatable {
    
    static func == (lhs: Device, rhs: Device) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
    
}
