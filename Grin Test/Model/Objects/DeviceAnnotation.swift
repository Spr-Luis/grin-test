//
//  DeviceAnnotation.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import Foundation
import MapKit

class DeviceAnnotation: NSObject, MKAnnotation {
        
    dynamic var coordinate: CLLocationCoordinate2D
    dynamic var title: String?
    dynamic var subtitle: String?
    var device: Device
    
    init(device: Device) {
        self.coordinate = device.coordinate
        self.title = device.name
        self.subtitle = device.strength
        self.device = device
    }
    
    func update(device: Device) {
        self.device = device
        self.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.001,
                                                 longitude: coordinate.longitude - 0.001)
        self.title = device.name
        self.subtitle = device.strength
    }
    
}
