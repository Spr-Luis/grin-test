//
//  AppDelegate.swift
//  Grin Test
//
//  Created by Luis Chávez on 12/20/18.
//  Copyright © 2018 Luis Chávez. All rights reserved.
//

import UIKit
import SwiftyBeaver

let log = SwiftyBeaver.self

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Log System - SwiftyBeaver
        log.addDestination(GrinConsoleDestination())

        // Root view start as a UITabBarController
        window?.rootViewController = UIStoryboard.rootTabBarController

        return true
    }

}
